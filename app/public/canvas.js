var Color, Controller, Effect, Fire, Plasma, Tunnel, canvas, controller, fps, fpsFilter, fx, lastUpdate, now, random, t, values,
  __hasProp = Object.prototype.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

random = function(n) {
  var i;
  i = Math.random();
  return Math.floor(i * n + 1);
};

window.requestAnimFrame = (function(callback) {
  return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(callback) {
    return window.setTimeout(callback, 1000 / 10);
  };
})();

fx = null;

values = null;

t = 0;

canvas = null;

fps = 0;

now = null;

lastUpdate = (new Date * 1) - 1;

fpsFilter = 10;

controller = null;

Controller = (function() {

  function Controller() {
    var ctx;
    this.lastMousePos = null;
    this.gui = new dat.GUI;
    this.canvas = canvas = document.getElementById("canvas");
    canvas.onmousemove = function(event) {
      var x, y;
      x = event.pageX - this.offsetLeft;
      y = event.pageY - this.offsetTop;
      return controller.lastMousePos = {
        x: x,
        y: y
      };
    };
    this.ctx = ctx = canvas.getContext("2d");
    this.w = parseInt(canvas.getAttribute("width"));
    this.h = parseInt(canvas.getAttribute("height"));
  }

  Controller.prototype.frame = function() {
    var frameFPS, h, w;
    if (canvas.getContext) {
      w = controller.w;
      h = controller.h;
      t++;
      fx.update(t);
      controller.ctx.clearRect(0, 0, w, h);
      fx.draw(t);
      frameFPS = 1000 / ((now = new Date) - lastUpdate);
      fps += (frameFPS - fps) / fpsFilter;
      lastUpdate = now;
      return requestAnimFrame(function() {
        return controller.frame(w, h);
      });
    }
  };

  return Controller;

})();

window.onload = function() {
  var elt, fpsOut, list, _fn, _i, _len, _ref;
  controller = new Controller;
  fpsOut = document.getElementById('fpsDiv');
  setInterval((function() {
    return fpsOut.innerHTML = fps.toFixed(1) + "fps";
  }), 1000);
  list = document.getElementById('list');
  _ref = ['Fire', 'Plasma', 'Tunnel'];
  _fn = function(elt) {
    var domelt;
    domelt = document.createElement('a');
    domelt.innerHTML = elt + " | ";
    domelt.onclick = function() {
      return window.init(elt);
    };
    domelt.setAttribute('href', '#');
    return list.appendChild(domelt);
  };
  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
    elt = _ref[_i];
    _fn(elt);
  }
  fx = new Plasma(controller, controller.w, controller.h);
  fx.addControllers();
  return controller.frame();
};

window.init = function(name) {
  var c, _fn, _i, _len, _ref;
  if (fx) {
    _ref = controller.gui.__controllers;
    _fn = function(c) {
      try {
        return controller.gui.remove(c);
      } catch (error) {

      }
    };
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      c = _ref[_i];
      _fn(c);
    }
  }
  fx = new window[name](controller, controller.w, controller.h);
  return fx.addControllers();
};

Color = function(r, g, b, a) {
  this.r = r;
  this.g = g;
  this.b = b;
  this.a = a;
};

Effect = (function() {

  function Effect(controller, w, h) {
    this.controller = controller;
    this.w = w;
    this.h = h;
    values = [];
    this.ctx = controller.ctx;
    this.palet = this.getPalet();
    this.imageData = this.ctx.createImageData(w, h);
    this.guiControllers = [];
  }

  Effect.prototype.addControllers = function() {};

  Effect.prototype.update = function() {};

  Effect.prototype.draw = function() {
    return this.ctx.putImageData(this.imageData, 0, 0);
  };

  Effect.prototype.set = function(x, y, c, a) {
    var index;
    if (a == null) a = null;
    if (!c) c = new Color(0, 0, 255, 255);
    index = (x + y * this.w) * 4;
    this.imageData.data[index + 0] = c.r;
    this.imageData.data[index + 1] = c.g;
    this.imageData.data[index + 2] = c.b;
    return this.imageData.data[index + 3] = c.a;
  };

  Effect.prototype.getPalet = function() {
    var a, b, c, g, i, r, res;
    res = [];
    c = null;
    r = 0;
    g = 0;
    b = 0;
    a = 255;
    res[0] = new Color(0, 0, 0, 255);
    for (i = 1; i <= 255; i++) {
      r = Math.min(255, Math.round(i * 10));
      g = Math.min(255, Math.round(i * 2));
      b = Math.min(255, Math.round(i / 3));
      c = new Color(r, g, b, a);
      res[i] = c;
    }
    return res;
  };

  return Effect;

})();

Fire = (function(_super) {

  __extends(Fire, _super);

  function Fire(ctx, w, h) {
    var i, j, v;
    Fire.__super__.constructor.apply(this, arguments);
    this.attenuation = 0.95;
    for (j = 0; 0 <= h ? j <= h : j >= h; 0 <= h ? j++ : j--) {
      values[j] = [];
      for (i = 0; 0 <= w ? i <= w : i >= w; 0 <= w ? i++ : i--) {
        v = 0;
        if (j === h - 1) v = random(256);
        values[j][i] = v;
      }
    }
  }

  Fire.prototype.update = function(time) {
    var i, j, lastMousePos, offset, _results;
    lastMousePos = controller.lastMousePos;
    if (lastMousePos !== null) {
      offset = 2;
      _results = [];
      for (i = -offset; -offset <= offset ? i <= offset : i >= offset; -offset <= offset ? i++ : i--) {
        _results.push((function() {
          var _results2;
          _results2 = [];
          for (j = -offset; -offset <= offset ? j <= offset : j >= offset; -offset <= offset ? j++ : j--) {
            if (lastMousePos.x + i < this.w && lastMousePos.x + i >= 0 && lastMousePos.y + j >= 0 && lastMousePos.y + j < this.h) {
              _results2.push(values[lastMousePos.y + j][lastMousePos.x + i] = random(255));
            } else {
              _results2.push(void 0);
            }
          }
          return _results2;
        }).call(this));
      }
      return _results;
    }
  };

  Fire.prototype.draw = function(time) {
    var i, j, nb, vinit, x, _ref, _ref2, _results;
    _results = [];
    for (i = 0, _ref = this.w - 1; 0 <= _ref ? i <= _ref : i >= _ref; 0 <= _ref ? i++ : i--) {
      vinit = 0;
      if (Math.random() > 0.5) vinit = 255;
      values[this.h - 1][i] = vinit;
      for (j = _ref2 = this.h - 2; _ref2 <= 0 ? j <= 0 : j >= 0; _ref2 <= 0 ? j++ : j--) {
        x = values[j][i];
        nb = 1;
        if (j < this.h - 1) {
          x += 2 * values[j + 1][i];
          nb += 2;
          if (i < this.w - 1) {
            x += values[j + 1][i + 1];
            nb++;
          }
          if (i > 0) {
            x += values[j + 1][i - 1];
            nb++;
          }
        }
        x = Math.floor(x * this.attenuation / nb);
        x = Math.min(255, x);
        x = Math.max(0, x);
        values[j][i] = x;
        this.set(i, j, this.palet[x]);
      }
      _results.push(Fire.__super__.draw.apply(this, arguments));
    }
    return _results;
  };

  Fire.prototype.addControllers = function() {
    return controller.gui.add(this, 'attenuation', 0.9, 1.1);
  };

  return Fire;

})(Effect);

Plasma = (function(_super) {

  __extends(Plasma, _super);

  function Plasma(ctx, w, h, speed) {
    var i, j, v, _ref, _ref2;
    this.w = w;
    this.h = h;
    this.speed = speed != null ? speed : 4;
    Plasma.__super__.constructor.apply(this, arguments);
    this.c1 = 1 / 9;
    this.c2 = 1 / 17;
    this.c3 = 1 / 8;
    this.s1 = this.speed / 99;
    this.s2 = this.speed / 27;
    this.s3 = this.speed / 30;
    Math.PI2 = 2 * Math.PI;
    for (j = 0, _ref = this.h; 0 <= _ref ? j <= _ref : j >= _ref; 0 <= _ref ? j++ : j--) {
      values[j] = [];
      for (i = 0, _ref2 = this.w; 0 <= _ref2 ? i <= _ref2 : i >= _ref2; 0 <= _ref2 ? i++ : i--) {
        v = random(256);
        values[j][i] = v;
      }
    }
    this.sins = [];
    for (i = 0; i <= 63; i++) {
      this.sins[i] = Math.sin(i / 10);
    }
    this.coss = [];
    for (i = 0; i <= 63; i++) {
      this.coss[i] = Math.cos(i / 10);
    }
  }

  Plasma.prototype.update = function(time) {
    var c1, c2, c3, i, ix, j, lastMousePos, offset, t1, t2, t3, val, x, _ref, _ref2;
    c1 = this.c1;
    c2 = this.c2;
    c3 = this.c3;
    t1 = this.s1 * time;
    t2 = this.s2 * time;
    t3 = this.s3 * time;
    for (i = 0, _ref = this.w; 0 <= _ref ? i <= _ref : i >= _ref; 0 <= _ref ? i++ : i--) {
      ix = Math.sin(i * c1 + t1);
      ix += Math.sin(i * c2 - t3);
      for (j = 0, _ref2 = this.h; 0 <= _ref2 ? j <= _ref2 : j >= _ref2; 0 <= _ref2 ? j++ : j--) {
        x = ix + Math.cos(j * c2 + t2);
        x += Math.cos(j * c3 - t3);
        x = x / 4;
        x = Math.round(((x + 1) / 2) * 255);
        values[j][i] = x;
        this.set(i, j, this.palet[x]);
      }
    }
    lastMousePos = controller.lastMousePos;
    if (lastMousePos !== null) {
      offset = 20;
      for (i = -offset; -offset <= offset ? i <= offset : i >= offset; -offset <= offset ? i++ : i--) {
        for (j = -offset; -offset <= offset ? j <= offset : j >= offset; -offset <= offset ? j++ : j--) {
          if (i * i + j * j > offset * offset) continue;
          if (lastMousePos.x + i < this.w && lastMousePos.x + i >= 0 && lastMousePos.y + j >= 0 && lastMousePos.y + j < this.h) {
            val = values[lastMousePos.y + j][lastMousePos.x + i];
            this.set(lastMousePos.x + i, lastMousePos.y + j, this.palet[Math.round(val + 40 * (Math.cos(i * c1) + Math.sin(j * c2)) % 256)]);
          }
        }
      }
    }
    return Plasma.__super__.update.apply(this, arguments);
  };

  Plasma.prototype.getPalet = function() {
    var a, b, c, g, i, r, res;
    res = [];
    c = null;
    r = 0;
    g = 0;
    b = 0;
    a = 255;
    res[0] = new Color(0, 0, 0, 255);
    for (i = 0; i <= 255; i++) {
      r = Math.round(128.0 + 128 * Math.sin(3.1415 * i / 16.0));
      g = Math.round(128.0 + 128 * Math.sin(3.1415 * i / 128.0));
      c = new Color(r, i, g, a);
      res[i] = c;
    }
    return res;
  };

  Plasma.prototype.addControllers = function() {
    controller.gui.add(this, 'c1', 0, 1);
    controller.gui.add(this, 'c2', 0, 1);
    return controller.gui.add(this, 'c3', 0, 1);
  };

  return Plasma;

})(Effect);

Tunnel = (function(_super) {

  __extends(Tunnel, _super);

  function Tunnel(controller, w, h) {
    var a, d, hsize, i, j, rx, ry, size, _ref, _ref2;
    this.w = w;
    this.h = h;
    Tunnel.__super__.constructor.apply(this, arguments);
    this.updatePlasma = false;
    this.speedZ = 5;
    this.speedAngle = 2;
    this.tilingAngle = 5;
    this.tilingDistance = 1;
    this.drawGradient = false;
    this.grad = this.ctx.createRadialGradient(this.w / 2, this.h / 2, 2, this.w / 2, this.h / 2, this.w / 2);
    this.grad.addColorStop(0, "rgba(0, 0, 15, 255)");
    this.grad.addColorStop(1, "rgba(255, 255, 255, 0)");
    this.offsetX = this.w / 2;
    this.offsetY = this.h / 2;
    this.plasma = new Plasma(controller, w * 2, h * 2);
    this.plasma.update(1);
    this.angles = [];
    this.distances = [];
    this.texture = [];
    this.tsize = size = this.w * 2;
    hsize = size / 2;
    for (j = 0, _ref = size - 1; 0 <= _ref ? j <= _ref : j >= _ref; 0 <= _ref ? j++ : j--) {
      this.distances[j] = [];
      this.angles[j] = [];
      for (i = 0, _ref2 = size - 1; 0 <= _ref2 ? i <= _ref2 : i >= _ref2; 0 <= _ref2 ? i++ : i--) {
        a = Math.atan2(-i + hsize, j - hsize) / Math.PI * hsize + hsize;
        this.angles[j][i] = a;
        rx = i - size / 2;
        ry = j - size / 2;
        d = (8388608 >> 5) / (rx * rx + ry * ry);
        if (d === Infinity) d = 1000000;
        this.distances[j][i] = d;
        this.set(i, j, new Color(0, 0, Math.round(d % 256), 255));
      }
    }
  }

  Tunnel.prototype.getPalet = function() {
    return [];
  };

  Tunnel.prototype.update = function(time) {
    var lastMousePos;
    if (this.updatePlasma) this.plasma.update(time);
    lastMousePos = controller.lastMousePos;
    if (lastMousePos !== null) {
      this.offsetX = Math.floor(lastMousePos.x / this.w * this.plasma.w * 0.5);
      return this.offsetY = lastMousePos.y;
    }
  };

  Tunnel.prototype.draw = function(time) {
    var a, d, i, j, _ref, _ref2;
    for (j = 0, _ref = this.h - 1; 0 <= _ref ? j <= _ref : j >= _ref; 0 <= _ref ? j++ : j--) {
      for (i = 0, _ref2 = this.w - 1; 0 <= _ref2 ? i <= _ref2 : i >= _ref2; 0 <= _ref2 ? i++ : i--) {
        d = Math.floor(this.tilingDistance * (this.speedZ * time + this.distances[j + this.offsetY][i + this.offsetX]) % this.plasma.w);
        a = Math.floor((this.tilingAngle * this.angles[j + this.offsetY][i + this.offsetX] + time * this.speedAngle) % this.plasma.h);
        this.set(i, j, this.plasma.palet[values[d][a]]);
      }
    }
    Tunnel.__super__.draw.apply(this, arguments);
    if (this.drawGradient) {
      this.ctx.fillStyle = this.grad;
      return this.ctx.fillRect(0, 0, this.w, this.h);
    }
  };

  Tunnel.prototype.addControllers = function() {
    controller.gui.add(this, 'speedZ', 0, 30);
    controller.gui.add(this, 'speedAngle', 0, 30);
    controller.gui.add(this, 'tilingAngle', 0, 15);
    controller.gui.add(this, 'tilingDistance', 0, 1);
    controller.gui.add(this, 'updatePlasma');
    return controller.gui.add(this, 'drawGradient');
  };

  return Tunnel;

})(Effect);
