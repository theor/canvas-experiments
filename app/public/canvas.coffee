random = (n) ->
  i = Math.random()
  Math.floor(i * n + 1)

window.requestAnimFrame = ((callback) ->
            window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            (callback) -> window.setTimeout(callback, 1000 / 10)
)();

fx = null
values = null
t = 0
canvas = null
fps = 0
now = null
lastUpdate = (new Date * 1) - 1
fpsFilter = 10

controller = null

class Controller
  constructor: ->
    this.lastMousePos = null
    this.gui = new dat.GUI
    this.canvas = canvas = document.getElementById("canvas");
    canvas.onmousemove =  (event) ->
      x = event.pageX - this.offsetLeft
      y = event.pageY - this.offsetTop
      controller.lastMousePos = {x: x, y: y}

    this.ctx = ctx = canvas.getContext("2d")
    this.w = parseInt(canvas.getAttribute("width"))
    this.h = parseInt(canvas.getAttribute("height"))

  frame: ->
    if (canvas.getContext)
      w = controller.w
      h = controller.h
      t++
      fx.update t
      controller.ctx.clearRect(0, 0, w, h)
      fx.draw t
      frameFPS = 1000 / ((now=new Date) - lastUpdate)
      fps += (frameFPS - fps) / fpsFilter;
      lastUpdate = now
      requestAnimFrame(() -> controller.frame(w, h))


window.onload = () ->
  controller = new Controller
  fpsOut = document.getElementById('fpsDiv');
  setInterval((() -> fpsOut.innerHTML = fps.toFixed(1) + "fps"), 1000)
  list = document.getElementById('list')
  ((elt)->
    domelt = document.createElement 'a'
    domelt.innerHTML = elt + " | "
    domelt.onclick = () ->window.init(elt)
    domelt.setAttribute 'href', '#'
    list.appendChild domelt
  ) elt for elt in ['Fire', 'Plasma', 'Tunnel']
  fx = new Plasma controller, controller.w, controller.h
  fx.addControllers()
  controller.frame()

window.init = (name) ->
  if fx
    ((c) ->
      try
        controller.gui.remove c
      catch error
    ) c for c in controller.gui.__controllers
    #((c) -> controller.gui.remove c) for c in fx.guiControllers
  fx = new window[name](controller, controller.w, controller.h)
  fx.addControllers()

Color = (@r, @g, @b, @a) ->

class Effect
  constructor: (@controller, @w, @h) ->
    values = []
    this.ctx = controller.ctx
    this.palet = this.getPalet()
    this.imageData = this.ctx.createImageData(w, h)
    this.guiControllers = []
  addControllers: ->

  update: ->

  draw: ->
    this.ctx.putImageData(this.imageData, 0, 0)
  set: (x, y, c, a = null) ->
    c = new Color(0, 0, 255, 255) if !c
    index = (x + y * this.w) * 4
    this.imageData.data[index+0] = c.r
    this.imageData.data[index+1] = c.g
    this.imageData.data[index+2] = c.b
    this.imageData.data[index+3] = (c.a)
  getPalet: ->
    res = []
    c = null
    r = 0
    g = 0
    b = 0
    a = 255
#    divide = Math.floor(255 / 3)
    res[0] = new Color(0, 0, 0, 255)
    for i in [1..255]
      r = Math.min(255, Math.round(i * 10))
      g = Math.min(255, Math.round(i * 2))
      b = Math.min(255, Math.round(i / 3))
      c = new Color(r, g, b, a)
      res[i] = c
    res


class Fire extends Effect
  constructor: (ctx, w, h) ->
    super
    this.attenuation = 0.95
    for j in [0..h]
      values[j] = []
      for i in [0..w]
        v = 0
        v = random(256) if j == h - 1
        values[j][i] = v

  update: (time) ->
    lastMousePos = controller.lastMousePos
    if lastMousePos != null
      offset = 2
      for i in [-offset..offset]
        for j in [-offset..offset]
          values[lastMousePos.y + j][lastMousePos.x + i] = random(255) if (lastMousePos.x+i < this.w && lastMousePos.x + i >= 0 && lastMousePos.y+j >= 0 && lastMousePos.y+j < this.h)

  draw: (time) ->
    for i in [0..this.w-1]
      vinit = 0
      vinit = 255 if Math.random() > 0.5
      values[this.h - 1][i] = vinit
      for j in [this.h-2..0]
        x = values[j][i]
        nb = 1
        if (j < this.h - 1)
          x += 2 * values[j + 1][i]
          nb += 2
          if (i < this.w - 1)
            x += values[j + 1][i + 1]
            nb++
          if (i > 0)
            x += values[j + 1][i - 1]
            nb++
#        x = Math.min(255, Math.floor(x / nb))
        x = Math.floor(x*this.attenuation/nb)
        x = Math.min(255, x)
        x = Math.max(0, x)
        values[j][i] = x
        this.set(i, j, this.palet[x])
      super
  addControllers: ->
    controller.gui.add this, 'attenuation', 0.9, 1.1

class Plasma extends Effect
  constructor: (ctx, @w, @h, @speed = 4) ->
    super
    this.c1 = 1/9
    this.c2 = 1/17
    this.c3 = 1/8
    this.s1 = this.speed / 99
    this.s2 = this.speed / 27
    this.s3 = this.speed / 30
    Math.PI2 = 2 * Math.PI

    for j in [0..this.h]
      values[j] = []
      for i in [0..this.w]
        v = random(256)
        values[j][i] = v
    this.sins = []
    for i in [0..63]
      this.sins[i] = Math.sin(i/10)
    this.coss = []
    for i in [0..63]
      this.coss[i] = Math.cos(i/10)


  update: (time) ->

    c1 = this.c1
    c2 = this.c2
    c3 = this.c3

    t1 = this.s1 * time
    t2 = this.s2 * time
    t3 = this.s3 * time

    for i in [0..this.w]
#      ix = this.sins[Math.round(10 * ((i*c1 + t1) % Math.PI2))]
#      i2 = Math.round(10 * ((i*c2 - t3) % Math.PI2))
#      i2 += 63 if i2 < 0
#      ix += this.sins[i2]
      ix = Math.sin (i*c1 + t1)
      ix += Math.sin (i*c2 - t3)

      for j in [0..this.h]
#        x = ix + this.coss[Math.round(10*((j*c2 + t2) % Math.PI2))]
#        j2 = Math.round(10 * ((j*c3 - t3) % Math.PI2))
#        j2 += 63 if j < 0
#        x += this.coss[j2]
        x = ix + Math.cos (j*c2 + t2)
        x += Math.cos (j*c3 - t3)
        x = x / 4
        x = Math.round(((x + 1) / 2) * 255)
        values[j][i] = x
        this.set(i, j, this.palet[x])
    lastMousePos = controller.lastMousePos
    if lastMousePos != null
      offset = 20
      for i in [-offset..offset]
        for j in [-offset..offset]
          continue if i*i + j*j > offset*offset
          if (lastMousePos.x+i < this.w && lastMousePos.x + i >= 0 && lastMousePos.y+j >= 0 && lastMousePos.y+j < this.h)
            val = values[lastMousePos.y + j][lastMousePos.x + i]
            this.set(lastMousePos.x + i, lastMousePos.y + j, this.palet[Math.round(val + 40 * (Math.cos(i*c1) + Math.sin(j*c2)) % 256)])
    super

  getPalet: ->
    res = []
    c = null
    r = 0
    g = 0
    b = 0
    a = 255
    res[0] = new Color(0, 0, 0, 255)
    for i in [0..255]
      r = Math.round(128.0 + 128 * Math.sin(3.1415 * i / 16.0))
      g = Math.round(128.0 + 128 * Math.sin(3.1415 * i / 128.0))
      c = new Color(r, i, g, a)
      res[i] = c
    res
  addControllers: ->
    controller.gui.add this, 'c1', 0, 1
    controller.gui.add this, 'c2', 0, 1
    controller.gui.add this, 'c3', 0, 1


class Tunnel extends Effect
  constructor: (controller, @w, @h) ->
    super
    this.updatePlasma = false
    this.speedZ = 5
    this.speedAngle = 2
    this.tilingAngle = 5
    this.tilingDistance = 1
    this.drawGradient = false

    this.grad = this.ctx.createRadialGradient(this.w/2, this.h/2, 2, this.w/2, this.h/2, this.w/2)
    this.grad.addColorStop(0, "rgba(0, 0, 15, 255)")
    this.grad.addColorStop(1, "rgba(255, 255, 255, 0)")

    this.offsetX = this.w/2
    this.offsetY = this.h/2

    this.plasma = new Plasma controller, w*2, h*2
    this.plasma.update 1
    this.angles = []
    this.distances = []
    this.texture = []

    this.tsize = size = this.w*2
    hsize = size / 2
    for j in [0..size-1]
      this.distances[j] = []
      this.angles[j] = []
      for i in [0..size-1]
        a = Math.atan2(-i+hsize, j-hsize) / (Math.PI) * hsize + hsize
        this.angles[j][i] = a
 #       this.set(i, j, new Color(0, 0, Math.round(a) * 2, 255))
        rx = i - size/2
        ry = j - size/2
        d = (8388608>>5) / (rx*rx + ry*ry)
        d = 1000000 if d == Infinity
        this.distances[j][i] = d
        this.set(i, j, new Color(0, 0, Math.round(d%256), 255))

  getPalet: -> []
  update: (time) ->
    this.plasma.update time if this.updatePlasma
    lastMousePos = controller.lastMousePos
    if lastMousePos != null
      this.offsetX = Math.floor(lastMousePos.x/this.w * this.plasma.w*0.5)
      this.offsetY = lastMousePos.y
  draw: (time) ->
    for j in [0..this.h-1]
      for i in [0..this.w-1]
        d = Math.floor(this.tilingDistance * (this.speedZ*time + this.distances[j+this.offsetY][i+this.offsetX]) % this.plasma.w)
        a = Math.floor((this.tilingAngle*this.angles[j+this.offsetY][i+this.offsetX] + time*this.speedAngle) % this.plasma.h)
#        this.set(i, j, this.texture[d][a])
        this.set(i, j, this.plasma.palet[values[d][a]])
    super
    if this.drawGradient
      this.ctx.fillStyle = this.grad
      this.ctx.fillRect(0, 0, this.w, this.h)
  addControllers: ->
    controller.gui.add this, 'speedZ', 0, 30
    controller.gui.add this, 'speedAngle', 0, 30
    controller.gui.add this, 'tilingAngle', 0, 15
    controller.gui.add this, 'tilingDistance', 0, 1
    controller.gui.add this, 'updatePlasma'
    controller.gui.add this, 'drawGradient'
